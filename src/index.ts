import NestedHtmlElementClass from "./NestedHtmlElement";
import template from "./NestedHtmlElement/template";

export default {
    NestedHtmlElement: NestedHtmlElementClass,
    NestedHtmlElementTemplate: template
};
