export default {
    form: `<div class="card-body" ref="{{ctx.nestedKey}}" id="{{ctx.instance.id}}-{{ctx.component.key}}">
    {{ctx.children}}
  </div>`
}
