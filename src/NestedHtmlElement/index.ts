import editForm from "./editForm";

interface Constructor<T> {
    new(component: any, options: any, data: any): T;
}

interface HtmlElementComponentBase {
    nestedKey: string;
    options: any;
    localRoot: any;
    component: any;
    loadRefs: (element: any, refs: any) => any;
    builderMode: boolean;
    previewMode: boolean;

    attach(element: any): any;

    render(element: any): any;

    detach(all: any): any;

    renderContent(): any;

    attachComponents(comps: any): any;

    renderComponents(components: any[]): any;
    checkRefreshOn(changes: any[], flags?: any): any;
    addComponents(data: any, options: any): void;
    redraw(): void;
    components: any[];
    refs: any;
    createComponent(component: any, options: any, data: any, before: any): any;
    sanitize(dirty: string): string;
}

export default function NestedHtmlElementClass<T extends HtmlElementComponentBase>(
    SuperClass: Constructor<T>
) {
    class NestedHtmlElement extends (<Constructor<HtmlElementComponentBase>>(
        SuperClass
    )) {
        private attachedElements: Element[];
        private attachedComponents: any;
        private element?: Element;
        private parentElement?: Element;

        public constructor(component: any, options: any, data: any) {
            super(component, options, data);
            this.attachedElements = [];
            this.attachedComponents = {};
            this.removeComponentId = this.removeComponentId.bind(this);
        }

        public static schema(...extend: object[]) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return super.schema(
                {
                    type: 'NestedHtmlElement',
                    key: 'NestedHtmlElement'
                },
                ...extend
            );
        }

        public static get builderInfo() {
            return {
                title: 'Nested HTML Element',
                group: 'advanced',
                icon: 'fa fa-code',
                weight: 110,
                schema: NestedHtmlElement.schema(),
            };
        }

        public checkRefreshOn(changes: any[], flags?: any) {
            super.checkRefreshOn(changes, flags);
            if(changes) {
                if (this.component.refreshOnChange || changes.every(c => this.components.indexOf(c.instance) !== -1)) {
                    for (const compId of Object.keys(this.attachedComponents)) {
                        this.attachedComponents[compId].component.redraw();
                    }
                }
            }
            if (!this.options.preview && !this.builderMode && this.component.refreshOnChange && this.element) {
                this.attachNestedElement();
            }
        }

        protected getChildComponents() {
            if (this.component.componentKeyRef) {
                const comp = this.localRoot.getComponent(this.component.componentKeyRef);
                if (comp) {
                    return [comp.component];
                }
            }
            return this.component.components;
        }

        addComponents(data: any, options: any) {
            if (!this.options.preview && !this.builderMode) {
                return;
            }
            super.addComponents(data, options);
        }

        findElements(element: Element) {
            return this.component.useDocumentSelectors ? document.querySelectorAll(this.component.selectors) : element.querySelectorAll(this.component.selectors);
        }

        deepCopy(data: any) {
            return JSON.parse(JSON.stringify(data));
        }

        removeComponentId(component: any) {
            if(component.id) {
                delete component.id
            }
            if(component.components) {
                component.components.forEach(this.removeComponentId);
            }
        }

        parseHTML(html: string) {
            let t = document.createElement('template');
            t.innerHTML = html;
            return t.content;
        }

        private attachNestedElement() {
            if(this.parentElement) {
                let attachedElements: Element[] = [];
                let newElements: Element[] = [];
                const foundElements = this.findElements(this.parentElement);
                foundElements.forEach(element => {
                    attachedElements.push(element);
                    if(this.attachedElements.indexOf(element) === -1) {
                        newElements.push(element);
                    }
                });
                let missingElements = [];
                for(const attachedElement of this.attachedElements) {
                    if(attachedElements.indexOf(attachedElement) === -1) {
                        missingElements.push(attachedElement);
                        for(const compId of Object.keys(this.attachedComponents)) {
                            if(this.attachedComponents[compId].element === attachedElement) {
                                this.attachedComponents[compId].component.element.remove();
                                this.attachedComponents[compId].component.detach();
                                this.components.splice(this.components.indexOf(this.attachedComponents[compId].component), 1);
                                delete this.attachedComponents[compId];
                            }
                        }
                    }
                }
                for (const missingElement of missingElements) {
                    this.attachedElements.splice(this.attachedElements.indexOf(missingElement), 1);
                }
                for(const element of newElements) {
                    for (let component of this.getChildComponents()) {
                        if (component.id && this.attachedComponents[component.id]) {
                            continue;
                        }
                        component = this.deepCopy(component);
                        this.removeComponentId(component);
                        const comp = this.createComponent(component, null, null, null);
                        let lastElement = this.parseHTML(this.sanitize(comp.render())).firstChild;
                        // @ts-ignore
                        element.append(lastElement);
                        comp.attach(lastElement);
                        this.attachedComponents[comp.id] = {
                            component: comp,
                            element
                        }
                    }
                    this.attachedElements.push(element);
                }
            }
        }

        renderComponents(components: any[]) {
            if(!this.options.preview && !this.builderMode) {
                return '';
            }
            return super.renderComponents(components);
        }

        public render(element: any): any {
            return super.render(element);
        }

        public static editForm = editForm;

        public attach(element: Element) {
            let all = [];
            all.push(super.attach(element));

            this.loadRefs(element, {
                [this.nestedKey]: 'single',
            });

            if (this.refs[this.nestedKey]) {
                if(!this.options.preview && !this.builderMode && this.element && this.element.parentElement) {
                    this.parentElement = this.element.parentElement;
                    this.refs[this.nestedKey].remove();
                    this.attachNestedElement();
                    element.remove();
                }
                all.push(this.attachComponents(this.refs[this.nestedKey]));
            }
            return Promise.all(all);
        }

        get templateName() {
            return NestedHtmlElement.schema().key;
        }

        public detach (all: any) {
            super.detach(all);
            this.attachedElements = [];
            for(const compId of Object.keys(this.attachedComponents)) {
                if(this.attachedComponents[compId].component.element) {
                    this.attachedComponents[compId].component.element.remove();
                }
            }
            this.attachedComponents = {};
        }
    }

    return NestedHtmlElement;
}
