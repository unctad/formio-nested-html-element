export default () => {
    return {
        "components": [
            {
                "type": "tabs",
                "key": "tabs",
                "components": [
                    {
                        "key": "display",
                        "components": [
                            {
                                "type": "textfield",
                                "input": true,
                                "key": "selectors",
                                "weight": 50,
                                "label": "Selectors",
                                "placeholder": "",
                                "tooltip": "This is a DOMString containing one or more selectors to match. This string must be a valid CSS. The document that matches the specified selector, or group of selectors.",
                                "validate": {
                                    "required": true
                                }
                            },
                            {
                                weight: 700,
                                type: 'checkbox',
                                label: 'Use document (body) for selectors',
                                key: 'useDocumentSelectors',
                                input: true
                            },
                            {
                                "type": "textfield",
                                "input": true,
                                "key": "componentKeyRef",
                                "weight": 50,
                                "label": "Component key reference",
                                "placeholder": ""
                            },
                            {
                                weight: 700,
                                type: 'checkbox',
                                label: 'Refresh On Change',
                                tooltip: 'Rerender the field whenever a value on the form changes.',
                                key: 'refreshOnChange',
                                input: true
                            }
                        ],
                        "label": "Display",
                        "weight": 0
                    },
                    {
                        "label": "API",
                        "key": "api",
                        "weight": 30,
                        "components": [
                            {
                                "weight": 0,
                                "type": "textfield",
                                "input": true,
                                "key": "key",
                                "label": "Property Name",
                                "tooltip": "The name of this field in the API endpoint.",
                                "validate": {
                                    "pattern": "(\\w|\\w[\\w-.]*\\w)",
                                    "patternMessage": "The property name must only contain alphanumeric characters, underscores, dots and dashes and should not be ended by dash or dot.",
                                    "required": true
                                }
                            },
                            {
                                "weight": 100,
                                "type": "tags",
                                "input": true,
                                "label": "Field Tags",
                                "storeas": "array",
                                "tooltip": "Tag the field for use in custom logic.",
                                "key": "tags"
                            },
                            {
                                "weight": 200,
                                "type": "datamap",
                                "label": "Custom Properties",
                                "tooltip": "This allows you to configure any custom properties for this component.",
                                "key": "properties",
                                "valueComponent": {
                                    "type": "textfield",
                                    "key": "value",
                                    "label": "Value",
                                    "placeholder": "Value",
                                    "input": true
                                }
                            }
                        ]
                    },
                    {
                        "key": "conditional",
                        "components": [
                            {
                                "type": "panel",
                                "title": "Advanced Conditions",
                                "theme": "default",
                                "collapsible": true,
                                "collapsed": true,
                                "key": "customConditionalPanel",
                                "weight": 110,
                                "components": [
                                    {
                                        "type": "htmlelement",
                                        "tag": "div",
                                        "content": "<p>The following variables are available in all scripts.</p><table class=\"table table-bordered table-condensed table-striped\"><tr><th>form</th><td>The complete form JSON object</td></tr><tr><th>submission</th><td>The complete submission object.</td></tr><tr><th>data</th><td>The complete submission data object.</td></tr><tr><th>row</th><td>Contextual \"row\" data, used within DataGrid, EditGrid, and Container components</td></tr><tr><th>component</th><td>The current component JSON</td></tr><tr><th>instance</th><td>The current component instance.</td></tr><tr><th>value</th><td>The current value of the component.</td></tr><tr><th>moment</th><td>The moment.js library for date manipulation.</td></tr><tr><th>_</th><td>An instance of <a href=\"https://lodash.com/docs/\" target=\"_blank\">Lodash</a>.</td></tr><tr><th>utils</th><td>An instance of the <a href=\"http://formio.github.io/formio.js/docs/identifiers.html#utils\" target=\"_blank\">FormioUtils</a> object.</td></tr><tr><th>util</th><td>An alias for \"utils\".</td></tr></table><br/>"
                                    },
                                    {
                                        "type": "panel",
                                        "title": "JavaScript",
                                        "collapsible": true,
                                        "collapsed": false,
                                        "style": {
                                            "margin-bottom": "10px"
                                        },
                                        "key": "customConditional-js",
                                        "components": [
                                            {
                                                "type": "textarea",
                                                "key": "customConditional",
                                                "rows": 5,
                                                "editor": "ace",
                                                "hideLabel": true,
                                                "as": "javascript",
                                                "input": true
                                            },
                                            {
                                                "type": "htmlelement",
                                                "tag": "div",
                                                "content": "<p>Enter custom javascript code.</p><p>You must assign the <strong>show</strong> variable a boolean result.</p><p><strong>Note: Advanced Conditional logic will override the results of the Simple Conditional logic.</strong></p><h5>Example</h5><pre>show = !!data.showMe;</pre>"
                                            }
                                        ]
                                    },
                                    {
                                        "type": "panel",
                                        "title": "JSONLogic",
                                        "collapsible": true,
                                        "collapsed": true,
                                        "key": "customConditional-json",
                                        "components": [
                                            {
                                                "type": "htmlelement",
                                                "tag": "div",
                                                "content": "<p>Execute custom logic using <a href=\"http://jsonlogic.com/\" target=\"_blank\">JSONLogic</a>.</p><p>Full <a href=\"https://lodash.com/docs\" target=\"_blank\">Lodash</a> support is provided using an \"_\" before each operation, such as <code>{\"_sum\": {var: \"data.a\"}}</code></p><p><a href=\"http://formio.github.io/formio.js/app/examples/conditions.html\" target=\"_blank\">Click here for an example</a></p>"
                                            },
                                            {
                                                "type": "textarea",
                                                "key": "conditional.json",
                                                "rows": 5,
                                                "editor": "ace",
                                                "hideLabel": true,
                                                "as": "json",
                                                "input": true
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "type": "panel",
                                "title": "Advanced Next Page",
                                "theme": "default",
                                "collapsible": true,
                                "collapsed": true,
                                "key": "nextPagePanel",
                                "weight": 110,
                                "components": [
                                    {
                                        "type": "htmlelement",
                                        "tag": "div",
                                        "content": "<p>The following variables are available in all scripts.</p><table class=\"table table-bordered table-condensed table-striped\"><tr><th>form</th><td>The complete form JSON object</td></tr><tr><th>submission</th><td>The complete submission object.</td></tr><tr><th>data</th><td>The complete submission data object.</td></tr><tr><th>row</th><td>Contextual \"row\" data, used within DataGrid, EditGrid, and Container components</td></tr><tr><th>component</th><td>The current component JSON</td></tr><tr><th>instance</th><td>The current component instance.</td></tr><tr><th>value</th><td>The current value of the component.</td></tr><tr><th>moment</th><td>The moment.js library for date manipulation.</td></tr><tr><th>_</th><td>An instance of <a href=\"https://lodash.com/docs/\" target=\"_blank\">Lodash</a>.</td></tr><tr><th>utils</th><td>An instance of the <a href=\"http://formio.github.io/formio.js/docs/identifiers.html#utils\" target=\"_blank\">FormioUtils</a> object.</td></tr><tr><th>util</th><td>An alias for \"utils\".</td></tr></table><br/>"
                                    },
                                    {
                                        "type": "panel",
                                        "title": "JavaScript",
                                        "collapsible": true,
                                        "collapsed": false,
                                        "style": {
                                            "margin-bottom": "10px"
                                        },
                                        "key": "nextPage-js",
                                        "components": [
                                            {
                                                "type": "textarea",
                                                "key": "nextPage",
                                                "rows": 5,
                                                "editor": "ace",
                                                "hideLabel": true,
                                                "as": "javascript",
                                                "input": true
                                            },
                                            {
                                                "type": "htmlelement",
                                                "tag": "div",
                                                "content": "<p>Enter custom javascript code.</p>\n  <p>You must assign the <strong>next</strong> variable with the API key of the next page.</p>\n  <p>The global variable <strong>data</strong> is provided, and allows you to access the data of any form component, by using its API key.</p>\n  <p>Also <strong>moment</strong> library is available, and allows you to manipulate dates in a convenient way.</p>\n  <h5>Example</h5><pre>next = data.addComment ? 'page3' : 'page4';</pre>\n"
                                            }
                                        ]
                                    },
                                    {
                                        "type": "panel",
                                        "title": "JSONLogic",
                                        "collapsible": true,
                                        "collapsed": true,
                                        "key": "nextPage-json",
                                        "components": [
                                            {
                                                "type": "htmlelement",
                                                "tag": "div",
                                                "content": "<p>Execute custom logic using <a href=\"http://jsonlogic.com/\" target=\"_blank\">JSONLogic</a>.</p><p>Full <a href=\"https://lodash.com/docs\" target=\"_blank\">Lodash</a> support is provided using an \"_\" before each operation, such as <code>{\"_sum\": {var: \"data.a\"}}</code></p>\n  <p>Submission data is available as JsonLogic variables, with the same api key as your components.</p>\n"
                                            },
                                            {
                                                "type": "textarea",
                                                "key": "nextPage",
                                                "rows": 5,
                                                "editor": "ace",
                                                "hideLabel": true,
                                                "as": "json",
                                                "input": true
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "type": "panel",
                                "title": "Simple",
                                "key": "simple-conditional",
                                "theme": "default",
                                "components": [
                                    {
                                        "type": "select",
                                        "input": true,
                                        "label": "This component should Display:",
                                        "key": "conditional.show",
                                        "dataSrc": "values",
                                        "data": {
                                            "values": [
                                                {
                                                    "label": "True",
                                                    "value": "true"
                                                },
                                                {
                                                    "label": "False",
                                                    "value": "false"
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        "type": "select",
                                        "input": true,
                                        "label": "When the form component:",
                                        "key": "conditional.when",
                                        "dataSrc": "custom",
                                        "valueProperty": "value",
                                        "data": {}
                                    },
                                    {
                                        "type": "textfield",
                                        "input": true,
                                        "label": "Has the value:",
                                        "key": "conditional.eq"
                                    }
                                ]
                            }
                        ],
                        "label": "Conditional",
                        "weight": 40
                    },
                    {
                        "label": "Logic",
                        "key": "logic",
                        "weight": 50,
                        "components": [
                            {
                                "weight": 0,
                                "input": true,
                                "label": "Advanced Logic",
                                "key": "logic",
                                "templates": {
                                    "header": "<div class=\"row\"> \n  <div class=\"col-sm-6\">\n    <strong>{{ value.length }} {{ ctx.t(\"Advanced Logic Configured\") }}</strong>\n  </div>\n</div>",
                                    "row": "<div class=\"row\"> \n  <div class=\"col-sm-6\">\n    <div>{{ row.name }} </div>\n  </div>\n  <div class=\"col-sm-2\"> \n    <div class=\"btn-group pull-right\"> \n      <div class=\"btn btn-default editRow\">{{ ctx.t(\"Edit\") }}</div> \n      <div class=\"btn btn-danger removeRow\">{{ ctx.t(\"Delete\") }}</div> \n    </div> \n  </div> \n</div>",
                                    "footer": ""
                                },
                                "type": "editgrid",
                                "addAnother": "Add Logic",
                                "saveRow": "Save Logic",
                                "components": [
                                    {
                                        "weight": 0,
                                        "input": true,
                                        "inputType": "text",
                                        "label": "Logic Name",
                                        "key": "name",
                                        "validate": {
                                            "required": true
                                        },
                                        "type": "textfield"
                                    },
                                    {
                                        "weight": 10,
                                        "key": "triggerPanel",
                                        "input": false,
                                        "title": "Trigger",
                                        "tableView": false,
                                        "components": [
                                            {
                                                "weight": 0,
                                                "input": true,
                                                "tableView": false,
                                                "components": [
                                                    {
                                                        "weight": 0,
                                                        "input": true,
                                                        "label": "Type",
                                                        "key": "type",
                                                        "tableView": false,
                                                        "data": {
                                                            "values": [
                                                                {
                                                                    "value": "simple",
                                                                    "label": "Simple"
                                                                },
                                                                {
                                                                    "value": "javascript",
                                                                    "label": "Javascript"
                                                                },
                                                                {
                                                                    "value": "json",
                                                                    "label": "JSON Logic"
                                                                },
                                                                {
                                                                    "value": "event",
                                                                    "label": "Event"
                                                                }
                                                            ]
                                                        },
                                                        "dataSrc": "values",
                                                        "template": "<span>{{ item.label }}</span>",
                                                        "type": "select"
                                                    },
                                                    {
                                                        "weight": 10,
                                                        "label": "",
                                                        "key": "simple",
                                                        "type": "container",
                                                        "tableView": false,
                                                        "components": [
                                                            {
                                                                "input": true,
                                                                "key": "show",
                                                                "label": "Show",
                                                                "type": "hidden",
                                                                "tableView": false
                                                            },
                                                            {
                                                                "type": "select",
                                                                "input": true,
                                                                "label": "When the form component:",
                                                                "key": "when",
                                                                "dataSrc": "custom",
                                                                "valueProperty": "value",
                                                                "tableView": false,
                                                                "data": {}
                                                            },
                                                            {
                                                                "type": "textfield",
                                                                "input": true,
                                                                "label": "Has the value:",
                                                                "key": "eq",
                                                                "tableView": false
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        "weight": 10,
                                                        "type": "textarea",
                                                        "key": "javascript",
                                                        "rows": 5,
                                                        "editor": "ace",
                                                        "as": "javascript",
                                                        "input": true,
                                                        "tableView": false,
                                                        "placeholder": "result = (data['mykey'] > 1);",
                                                        "description": "\"row\", \"data\", and \"component\" variables are available. Return \"result\"."
                                                    },
                                                    {
                                                        "weight": 10,
                                                        "type": "textarea",
                                                        "key": "json",
                                                        "rows": 5,
                                                        "editor": "ace",
                                                        "label": "JSON Logic",
                                                        "as": "json",
                                                        "input": true,
                                                        "tableView": false,
                                                        "placeholder": "{ ... }",
                                                        "description": "\"row\", \"data\", \"component\" and \"_\" variables are available. Return the result to be passed to the action if truthy."
                                                    },
                                                    {
                                                        "weight": 10,
                                                        "type": "textfield",
                                                        "key": "event",
                                                        "label": "Event Name",
                                                        "placeholder": "event",
                                                        "description": "The event that will trigger this logic. You can trigger events externally or via a button.",
                                                        "tableView": false
                                                    }
                                                ],
                                                "key": "trigger",
                                                "type": "container"
                                            }
                                        ],
                                        "type": "panel"
                                    },
                                    {
                                        "weight": 20,
                                        "input": true,
                                        "label": "Actions",
                                        "key": "actions",
                                        "tableView": false,
                                        "templates": {
                                            "header": "<div class=\"row\"> \n  <div class=\"col-sm-6\"><strong>{{ value.length }} {{ ctx.t(\"actions\") }}</strong></div>\n</div>",
                                            "row": "<div class=\"row\"> \n  <div class=\"col-sm-6\">\n    <div>{{ row.name }} </div>\n  </div>\n  <div class=\"col-sm-2\"> \n    <div class=\"btn-group pull-right\"> \n      <div class=\"btn btn-default editRow\">{{ ctx.t(\"Edit\") }}</div> \n      <div class=\"btn btn-danger removeRow\">{{ ctx.t(\"Delete\") }}</div> \n    </div> \n  </div> \n</div>",
                                            "footer": ""
                                        },
                                        "type": "editgrid",
                                        "addAnother": "Add Action",
                                        "saveRow": "Save Action",
                                        "components": [
                                            {
                                                "weight": 0,
                                                "title": "Action",
                                                "input": false,
                                                "key": "actionPanel",
                                                "type": "panel",
                                                "components": [
                                                    {
                                                        "weight": 0,
                                                        "input": true,
                                                        "inputType": "text",
                                                        "label": "Action Name",
                                                        "key": "name",
                                                        "validate": {
                                                            "required": true
                                                        },
                                                        "type": "textfield"
                                                    },
                                                    {
                                                        "weight": 10,
                                                        "input": true,
                                                        "label": "Type",
                                                        "key": "type",
                                                        "data": {
                                                            "values": [
                                                                {
                                                                    "value": "property",
                                                                    "label": "Property"
                                                                },
                                                                {
                                                                    "value": "value",
                                                                    "label": "Value"
                                                                },
                                                                {
                                                                    "label": "Merge Component Schema",
                                                                    "value": "mergeComponentSchema"
                                                                },
                                                                {
                                                                    "label": "Custom Action",
                                                                    "value": "customAction"
                                                                }
                                                            ]
                                                        },
                                                        "dataSrc": "values",
                                                        "template": "<span>{{ item.label }}</span>",
                                                        "type": "select"
                                                    },
                                                    {
                                                        "weight": 20,
                                                        "type": "select",
                                                        "template": "<span>{{ item.label }}</span>",
                                                        "dataSrc": "json",
                                                        "tableView": false,
                                                        "data": {
                                                            "json": [
                                                                {
                                                                    "label": "Hidden",
                                                                    "value": "hidden",
                                                                    "type": "boolean"
                                                                },
                                                                {
                                                                    "label": "Required",
                                                                    "value": "validate.required",
                                                                    "type": "boolean"
                                                                },
                                                                {
                                                                    "label": "Disabled",
                                                                    "value": "disabled",
                                                                    "type": "boolean"
                                                                },
                                                                {
                                                                    "label": "Label",
                                                                    "value": "label",
                                                                    "type": "string"
                                                                },
                                                                {
                                                                    "label": "Title",
                                                                    "value": "title",
                                                                    "type": "string"
                                                                },
                                                                {
                                                                    "label": "Prefix",
                                                                    "value": "prefix",
                                                                    "type": "string"
                                                                },
                                                                {
                                                                    "label": "Suffix",
                                                                    "value": "suffix",
                                                                    "type": "string"
                                                                },
                                                                {
                                                                    "label": "Tooltip",
                                                                    "value": "tooltip",
                                                                    "type": "string"
                                                                },
                                                                {
                                                                    "label": "Description",
                                                                    "value": "description",
                                                                    "type": "string"
                                                                },
                                                                {
                                                                    "label": "Placeholder",
                                                                    "value": "placeholder",
                                                                    "type": "string"
                                                                },
                                                                {
                                                                    "label": "Input Mask",
                                                                    "value": "inputMask",
                                                                    "type": "string"
                                                                },
                                                                {
                                                                    "label": "CSS Class",
                                                                    "value": "className",
                                                                    "type": "string"
                                                                },
                                                                {
                                                                    "label": "Container Custom Class",
                                                                    "value": "customClass",
                                                                    "type": "string"
                                                                }
                                                            ]
                                                        },
                                                        "key": "property",
                                                        "label": "Component Property",
                                                        "input": true
                                                    },
                                                    {
                                                        "weight": 30,
                                                        "input": true,
                                                        "label": "Set State",
                                                        "key": "state",
                                                        "tableView": false,
                                                        "data": {
                                                            "values": [
                                                                {
                                                                    "label": "True",
                                                                    "value": "true"
                                                                },
                                                                {
                                                                    "label": "False",
                                                                    "value": "false"
                                                                }
                                                            ]
                                                        },
                                                        "dataSrc": "values",
                                                        "template": "<span>{{ item.label }}</span>",
                                                        "type": "select"
                                                    },
                                                    {
                                                        "weight": 30,
                                                        "type": "textfield",
                                                        "key": "text",
                                                        "label": "Text",
                                                        "inputType": "text",
                                                        "input": true,
                                                        "tableView": false,
                                                        "description": "Can use templating with {{ data.myfield }}. \"data\", \"row\", \"component\" and \"result\" variables are available."
                                                    },
                                                    {
                                                        "weight": 20,
                                                        "input": true,
                                                        "label": "Value (Javascript)",
                                                        "key": "value",
                                                        "editor": "ace",
                                                        "as": "javascript",
                                                        "rows": 5,
                                                        "placeholder": "value = data.myfield;",
                                                        "type": "textarea",
                                                        "tableView": false,
                                                        "description": "\"row\", \"data\", \"component\", and \"result\" variables are available. Return the value."
                                                    },
                                                    {
                                                        "weight": 20,
                                                        "input": true,
                                                        "label": "Schema Defenition",
                                                        "key": "schemaDefinition",
                                                        "editor": "ace",
                                                        "as": "javascript",
                                                        "rows": 5,
                                                        "placeholder": "schema = { label: 'Updated' };",
                                                        "type": "textarea",
                                                        "tableView": false,
                                                        "description": "\"row\", \"data\", \"component\", and \"result\" variables are available. Return the schema."
                                                    },
                                                    {
                                                        "type": "htmlelement",
                                                        "tag": "div",
                                                        "content": "<p>The following variables are available in all scripts.</p><table class=\"table table-bordered table-condensed table-striped\"><tr><th>input</th><td>The value that was input into this component</td></tr><tr><th>form</th><td>The complete form JSON object</td></tr><tr><th>submission</th><td>The complete submission object.</td></tr><tr><th>data</th><td>The complete submission data object.</td></tr><tr><th>row</th><td>Contextual \"row\" data, used within DataGrid, EditGrid, and Container components</td></tr><tr><th>component</th><td>The current component JSON</td></tr><tr><th>instance</th><td>The current component instance.</td></tr><tr><th>value</th><td>The current value of the component.</td></tr><tr><th>moment</th><td>The moment.js library for date manipulation.</td></tr><tr><th>_</th><td>An instance of <a href=\"https://lodash.com/docs/\" target=\"_blank\">Lodash</a>.</td></tr><tr><th>utils</th><td>An instance of the <a href=\"http://formio.github.io/formio.js/docs/identifiers.html#utils\" target=\"_blank\">FormioUtils</a> object.</td></tr><tr><th>util</th><td>An alias for \"utils\".</td></tr></table><br/>"
                                                    },
                                                    {
                                                        "weight": 20,
                                                        "input": true,
                                                        "label": "Custom Action (Javascript)",
                                                        "key": "customAction",
                                                        "editor": "ace",
                                                        "rows": 5,
                                                        "placeholder": "value = data.myfield;",
                                                        "type": "textarea",
                                                        "tableView": false
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "label": "Layout",
                        "key": "layout",
                        "weight": 60,
                        "components": [
                            {
                                "label": "HTML Attributes",
                                "type": "datamap",
                                "input": true,
                                "key": "attributes",
                                "keyLabel": "Attribute Name",
                                "valueComponent": {
                                    "type": "textfield",
                                    "key": "value",
                                    "label": "Attribute Value",
                                    "input": true
                                },
                                "tooltip": "Provide a map of HTML attributes for component's input element (attributes provided by other component settings or other attributes generated by form.io take precedence over attributes in this grid)",
                                "addAnother": "Add Attribute"
                            },
                            {
                                "type": "panel",
                                "legend": "PDF Overlay",
                                "title": "PDF Overlay",
                                "key": "overlay",
                                "tooltip": "The settings inside apply only to the PDF forms.",
                                "weight": 2000,
                                "collapsible": true,
                                "collapsed": true,
                                "components": [
                                    {
                                        "type": "textfield",
                                        "input": true,
                                        "key": "overlay.style",
                                        "label": "Style",
                                        "placeholder": "",
                                        "tooltip": "Custom styles that should be applied to this component when rendered in PDF."
                                    },
                                    {
                                        "type": "textfield",
                                        "input": true,
                                        "key": "overlay.page",
                                        "label": "Page",
                                        "placeholder": "",
                                        "tooltip": "The PDF page to place this component."
                                    },
                                    {
                                        "type": "textfield",
                                        "input": true,
                                        "key": "overlay.left",
                                        "label": "Left",
                                        "placeholder": "",
                                        "tooltip": "The left margin within a page to place this component."
                                    },
                                    {
                                        "type": "textfield",
                                        "input": true,
                                        "key": "overlay.top",
                                        "label": "Top",
                                        "placeholder": "",
                                        "tooltip": "The top margin within a page to place this component."
                                    },
                                    {
                                        "type": "textfield",
                                        "input": true,
                                        "key": "overlay.width",
                                        "label": "Width",
                                        "placeholder": "",
                                        "tooltip": "The width of the component (in pixels)."
                                    },
                                    {
                                        "type": "textfield",
                                        "input": true,
                                        "key": "overlay.height",
                                        "label": "Height",
                                        "placeholder": "",
                                        "tooltip": "The height of the component (in pixels)."
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                "type": "hidden",
                "key": "type"
            }
        ]
    };
};
