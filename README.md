# Formio nested html element component

Register component
```js
import FormioNestedHtmlElement from 'formio-nested-html-element';
const FormioNestedHtmlElementComponent = FormioNestedHtmlElement.NestedHtmlElement(Components.components.panel);
// @ts-ignore
window.Formio.registerComponent(FormioNestedHtmlElementComponent.schema().key, FormioNestedHtmlElementComponent);
Templates.templates.bootstrap[FormioNestedHtmlElementComponent.schema().key] = FormioNestedHtmlElement.NestedHtmlElementTemplate;
```
